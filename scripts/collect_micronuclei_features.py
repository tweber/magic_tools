import os
#if os.uname().nodename == 'seneca.embl.de':
#    os.environ['OPENBLAS_NUM_THREADS'] = '8'
import base64
import json
import requests
from tqdm import tqdm
from io import StringIO

import pandas as pd
from skimage.io import imsave

from magic_tools.analysis.image import ImageLoader
from magic_tools.server.API_utils import ImgJson, decode_img, encode_img

################################################
# CONFIGURE PATHS MANUALLY
################################################

server_url = 'http://127.0.0.1:10123/'
config_path = '/path/to/magic_tools/resources/configurations/test_config_options_watershed.json'
mip_path = '/path/to/image/folder/*.tif'
savepath = '/path/to/output/folder/'

################################################

# set server configuration
requests.post(server_url+'set_config', json={'filepath': config_path})

# load images
loader = ImageLoader({'c': {'mip': mip_path}})

# batch processing
for imgset in tqdm(loader):
    if os.path.exists(savepath+'labels_nucl_'+imgset.name):
        continue

    # get image
    img = imgset.channels['mip']

    byte_content = base64.b64encode(img)  # to bytes
    string_content = byte_content.decode('utf-8')  # to string

    # request
    json_req = {'filename': imgset.name,
                'img': string_content,
                'width': img.shape[1],
                'height': img.shape[0],
                'dtype': img.dtype.str}

    #json_req = encode_img(img).dict()

    res = requests.post(server_url+'mni_features_watershed_full', json=json_req)
    
    # process output & save
    output = json.loads(res.content.decode('utf-8'))

    df = pd.read_csv(StringIO(output['mni_fts']), index_col=0)
    df.to_csv(savepath+'features_mni_'+imgset.name[:-4]+'.csv')

    mni = ImgJson(**output['mni'])
    mni_img = decode_img(img_string=mni.img, h=mni.height, w=mni.width, dtype=mni.dtype)
    nucl = ImgJson(**output['nucl'])
    nucl_img = decode_img(img_string=nucl.img, h=nucl.height, w=nucl.width, dtype=nucl.dtype)
    raw = ImgJson(**output['raw'])
    raw_img = decode_img(img_string=raw.img, h=raw.height, w=raw.width, dtype=raw.dtype)
    
    imsave(arr=raw_img, fname=savepath+'labels_raw_'+imgset.name, check_contrast=False)
    imsave(arr=mni_img, fname=savepath+'labels_mni_'+imgset.name, check_contrast=False)
    imsave(arr=nucl_img, fname=savepath+'labels_nucl_'+imgset.name, check_contrast=False)

    