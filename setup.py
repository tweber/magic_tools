from setuptools import setup, find_packages  

setup(
    name = 'magic_tools',
    packages = find_packages(),
    author='Marco R. Cosenza',
    author_email='marco.cosenza@embl.de',
    version='0.0.1'
    )
