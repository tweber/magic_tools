import json
import os
import logging

from ..analysis.features import WorkerCollection

class Configuration():
    def __init__(self, config_file: str = None):
           
        self.name = None
        self.workers = None
        self.options = None

        if config_file is not None:
            self.load_config(config_file)

    def load_config(self, config_file):
        if not os.path.exists(config_file):
            raise ValueError('file does not exist: {}'.format(config_file))
        with open(config_file, 'rb') as f:
            config = json.load(f)
            logging.debug('CONFIG {}'.format(str(config)))

        if 'name' in config.keys():
            self.name = config['name']
        if 'workers' in config.keys():
            self.workers = self.load_worker_collection(config['workers'])
        if 'options' in config.keys():
            self.options = config['options']

        


    def load_worker_collection(self, config):
        n_cpus = config['n_cpus']
        collection_dict = config['collection_dict']
        collection = WorkerCollection(n_cpus=n_cpus, collection_dict=collection_dict)
        return collection


class Pipeline():
    def __init__(self, config: Configuration):
        self.config = config
        self.options = config.options
        self.workers = config.workers

        for ext_type, worker in self.workers.workers.items():
            setattr(self, ext_type, worker)
        
        self.output = {}

    def run(self, input):
        raise NotImplementedError()

    def add_output(self, key, content):
        if key in self.output.keys():
            raise ValueError('key already exists in output')
        self.output[key] = content

    def get_output(self, key):
        return self.output[key]
        