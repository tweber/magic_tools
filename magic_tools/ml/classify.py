from matplotlib import pyplot as plt
import numpy as np
import pickle
import pandas as pd
from sklearn.metrics import accuracy_score, average_precision_score, balanced_accuracy_score, precision_recall_curve 
from sklearn.metrics import matthews_corrcoef, precision_score, recall_score, roc_auc_score
from sklearn.metrics import coverage_error, label_ranking_average_precision_score, label_ranking_loss, log_loss
from sklearn.model_selection import train_test_split
import xgboost as xgb
import os
from itertools import product

if 'jupyter' in os.environ['_']:
    import shap
    shap.initjs()


class Training():
    def __init__(self, params=None, 
                 dataset=None, features_cols=None, metadata_cols=None, label_col=None, 
                 random_state=None):
        '''Training class for the XGB models on tabular data for both semantic pixel classification and object classification.
        It supports both binary and multi-class classification training. 
        The class takes care of preparing and splitting the dataset, training the classifier and calculating performance scores.

        Parameters
        ----------
        params: dict, containing the parameters for the XGB model
        
        dataset: pandas.Dataframe, complete dataframe with features, metadata and labels, before splitting

        feature_cols: list of str, names of the feature columns as in the dataset, 
                      optional, check parse_dataset_cols() for more info

        metadata_cols: list of str, names of the metadata columns as in the dataset
                       optional, check parse_dataset_cols() for more info

        label_col: str, name of the label column as in the dataset, defaults to 'label'
                   optional, check parse_dataset_cols() for more info

        random_state: int, integer used as random seed in generating the classifier and splitting datasets
        '''
        # dataset
        self.dataset = dataset
        self.label_col = label_col
        self.metadata_cols = metadata_cols
        self.features_cols = features_cols
        self.parse_dataset_cols()

        # split
        self.training_set = None
        self.validation_set = None
        self.test_set = None
        self.splits = [0.8, 0.1, 0.1]
        
        # training
        self.params = params
        self.verbose = 0
        self.scores = None

        # mode
        if self.params['objective'] in ['binary:logistic']:
            self.mode = 'binary'
        elif self.params['objective'] in ['multi:softmax', 'multi:softprob']:
            self.mode = 'multi'
        else:
            self.mode = None

        # classifier parameters
        self.random_state = random_state
        self.params = params
        if self.params['seed'] is None: 
            self.params['seed'] = self.random_state
        
        # metric
        self._metrics_dict = {
            'binary': ['aucpr', 'map'],
            'multi': ['merror']
        }
        if self.params['eval_metric'] is None:
            self.params['eval_metric'] = self._metrics_dict[self.mode]
            self.eval_metrics = self._metrics_dict[self.mode]
        else:
            self.eval_metrics = self.params['eval_metric']

        self.classifier = xgb.XGBClassifier(**self.params)

        

    def parse_dataset_cols(self):
        '''Infer metadata, label and feature columns from the dataset:
        If label is not specified, 'label' is considered the default.
        If feature columns are not specified, they are considered as all columns except label and metadata columns.
        If metadata columns are not specified, if is considered there are no metadata column.'''

        self.label_col = 'label' if self.label_col is None else self.label_col
        
        if self.metadata_cols is None:
            self.metadata_cols = []
            self.features_cols = self.dataset.columns.difference([self.label_col]).to_list()
        elif self.features_cols is None:
            self.features_cols = self.dataset.columns.difference([self.label_col] + self.metadata_cols).to_list()
        
        col_set = [self.label_col]+self.features_cols+self.metadata_cols
        for col in col_set:
            if col not in self.dataset.columns:
                raise ValueError('column {} not in dataset'.format(col))

    def split_dataset(self, splits=None):
        '''Split dataset in training, validation and, optionally, test sets. Splits are stratified by the label column.
        
        Parameters
        ----------
        splits: list of floats, indicating the percentage of each split. Their sum equals to 1.'''

        if splits is not None:
            assert np.sum(splits) == 1
            self.splits = splits 
        
        # training split
        self.training_set, nottrain_set = train_test_split(
            self.dataset, 
            test_size = np.sum(self.splits[1:]), 
            random_state = self.random_state, 
            stratify = self.dataset[self.label_col])
        
        # validation and test splits
        if len(self.splits) >2:
            self.validation_set, self.test_set = train_test_split(
                nottrain_set, 
                test_size = self.splits[-1]/np.sum(self.splits[1:]), 
                random_state = self.random_state, 
                stratify = nottrain_set[self.label_col])
        else:
            self.validation_set = nottrain_set
            self.test_set = None

    def train(self):
        '''Train classifier and calculate scores on validation set.
        First a K-fold cross-validation training is performed with K=4, to get the right number of estimators.
        Then a classifier is trained on the entire dataset.
        Binary and multi-class classification are supported.'''

        self.scores = None
        xgtrain = xgb.DMatrix(self.training_set[self.features_cols].values, label = self.training_set[self.label_col].values)

        # estimate best number of iterations
        cvresult = xgb.cv(self.params, xgtrain, 
                          num_boost_round=self.classifier.get_params()['n_estimators'], 
                          nfold=4,
                          metrics=self.eval_metrics, # needs to be manually specified
                          early_stopping_rounds=50, 
                          verbose_eval=self.verbose,
                          show_stdv=True)
        self.classifier.set_params(n_estimators=cvresult.shape[0])

        # train classifier with estimated iterations and no cross-validation
        self.classifier.fit(self.training_set[self.features_cols], self.training_set[self.label_col], 
                            eval_set=[(self.validation_set[self.features_cols], self.validation_set[self.label_col])])
        
        # calculate scores
        self.calculate_scores(mode='validation')
        if self.verbose:
            print(self.scores)

    def calculate_scores(self, mode='validation'):
        '''Calculate performance scores for the classifier.
        Binary and multi-class scores are supported.
        
        Parameters
        ----------
        mode: str, choose from calculating scores on 'validation' or 'test' set'''

        if self.classifier is None:
            raise RuntimeError('Classifier has not been trained yet')

        dset_dict = {'validation': self.validation_set, 'test': self.test_set}
        dset = dset_dict[mode][self.features_cols]
        GT = dset_dict[mode][self.label_col].values
        dtest_predictions = self.classifier.predict(dset)
        dtest_predprob = self.classifier.predict_proba(dset)
        
        # different score panels depending if binary or multi-class classification
        scores = {}
        if self.mode == 'binary':
            scores[f'{mode}_accuracy'] =            accuracy_score(         GT, dtest_predictions)
            scores[f'{mode}_balanced_accuracy'] =   balanced_accuracy_score(GT, dtest_predictions)
            scores[f'{mode}_roc_auc'] =             roc_auc_score(          GT, dtest_predprob[:,1])
            for t in np.arange(0.1,1.1,0.01):
                scores[f'{mode}_precision_{t}'] =   precision_score(        GT, dtest_predprob[:,1] > t, average='binary')
                scores[f'{mode}_recall_{t}'] =      recall_score(           GT, dtest_predprob[:,1] > t, average='binary')
                scores[f'{mode}_average_precision'] =   average_precision_score(GT, dtest_predprob[:,1])
                scores[f'{mode}_matthews_coeff_{t}'] =  matthews_corrcoef(      GT, dtest_predprob[:,1] > t)
            p, r, t = precision_recall_curve(GT, dtest_predprob[:,1])
            fig, ax = plt.subplots(figsize=(7,7))
            ax.plot(t, p[:-1], color='lightblue', linewidth=2)
            ax.plot(t, r[:-1], color='orange', linewidth=2)
            fig.show()
        
        elif self.mode == 'multi':
            pred_argmax = np.argmax(dtest_predprob, axis=1)
            GT_onehot = pd.get_dummies(GT).values
            scores[f'{mode}_accuracy'] = accuracy_score(GT, pred_argmax)
            scores[f'{mode}_balanced_accuracy'] = balanced_accuracy_score(GT, pred_argmax)
            scores[f'{mode}_roc_auc'] = roc_auc_score(GT, dtest_predprob, average='weighted', multi_class='ovr')
            scores[f'{mode}_coverage_error'] = coverage_error(GT_onehot, dtest_predprob)
            scores[f'{mode}_label_ranking_average_precision'] = label_ranking_average_precision_score(GT_onehot, dtest_predprob)
            scores[f'{mode}_label_ranking_loss'] = label_ranking_loss(GT_onehot, dtest_predprob)
            scores[f'{mode}_log_loss'] = log_loss(GT_onehot, dtest_predprob)

        self.scores = scores

    def get_classifier(self):
        '''Get the trained classifier, together with the user-defined parameters and calculated performance scores.'''

        clf = Classifier(self.classifier, params=self.params, scores=self.scores)
        return clf
        
    def shap(self):
        '''Use SHapley Additive exPlanations (SHAP) to explain the output of the classifier.
        It generates a summary plot displaying feature importance for the output.'''

        explainer = shap.TreeExplainer(self.classifier)
        shap_values = explainer.shap_values(self.training_set[self.features_cols], check_additivity=False)
        shap.summary_plot(shap_values, self.features_cols, plot_type='bar')
    
    def optimize(self, opt_dict, savepath=None):
        keys = opt_dict.keys()
        combos = product(*opt_dict.values())

        opt_df = pd.DataFrame()
        for combo in combos:
            for n,k in enumerate(keys):
                self.params[k] = combo[n]
                
            self.classifier.set_params(**self.params)
            self.train()
            opt_row = {**self.params, **self.scores}
            opt_df = opt_df.append(opt_row, ignore_index=True)
            if savepath is not None:
                pd.DataFrame(opt_row).to_csv(savepath, mode='a', header=not os.path.exists(savepath))
        return opt_df


class Classifier():
    def __init__(self, classifier, params=None, scores=None):
        self.classifier = self.parse_classifier(classifier)

        try:
            self.features = self.classifier.get_booster().feature_names
        except:
            self.features =  self.classifier.feature_names_in_
            
        self.params = params
        self.scores = scores

    def predict(self, dataframe):
        dataframe = dataframe.loc[:,self.features]
        proba_out = self.classifier.predict_proba(dataframe)
        return proba_out

    def parse_classifier(self, classifier):
        if isinstance(classifier, str):
            if '.json' in classifier:
                return xgb.Booster().load_model(classifier)
            else:
                with open(classifier, 'rb') as file:
                    classifier = pickle.load(file)
                    return classifier
        elif isinstance(classifier, xgb.XGBClassifier):
            return classifier


def get_pixel_features(feature_names):
    '''Generate a feature dictionary from column names of a dataset, 
    as generated by a PixelFeatureExtractor instance.
    The output can be used to regenerate a PixelFeatureExtractor.
    
    Parameters
    ----------
    
    feature_names: list of str, features names as in the columns of the dataset
    
    Returns
    -------
    fts_dict: dict, the feature dictionary'''
    
    fts_dict = {'gaussian': [],
                'gradient': [],
                'laplacian': [],
                'hessian': []}
    
    for ft in feature_names:
        tokens = ft.split('_')
        sigma = tokens[1]
        filt = tokens[2]

        if fts_dict[filt] == []:
            fts_dict[filt] = [sigma]
        elif sigma not in fts_dict[filt]:
            fts_dict[filt].append(sigma)
    return fts_dict