import napari
import numpy as np
from tqdm.notebook import tqdm
from ..analysis.image import CropRoi
from ..analysis.display import roimat
from qtpy.QtWidgets import QPushButton
import scipy.ndimage as ndi
import pandas as pd

# semantic annotation
class ImageAnnotation():
    def __init__(self, loader, annotation_layer = None):
        self.loader = loader
        self.display_mode = 'channel'
        self.annotation_df = None
        self.annotation_basenames = None
        
        self.annotation_layer = annotation_layer
        if self.annotation_layer is None:
            chs = list(self.loader.dict_paths['channels'].keys())
            if len(chs) > 1:
                raise ValueError('Multiple channels are present, specify annotation_layer: {}'.format(chs))
            else:
                self.annotation_layer = chs[0]
        
        self.annotation_sets = []
        
    def set_annotation_round(self):
        self.annotation_basenames = self.loader.filepath_table.loc[:,'basename']
        self.n_mats = self.annotation_basenames.shape[0]
        for n, img in enumerate(self.loader):
            channel = img.__getattribute__(self.annotation_layer)
            mask = np.zeros_like(channel)
            ann = np.zeros_like(channel)
            self.annotation_sets.append(AnnotationSet(mat_id=n,
                                                      channel=channel, 
                                                      mask=mask, 
                                                      annotation=ann))
        self.curr_mat = -1
    
    def get_annotation_set(self, n=None):
        n = self.curr_mat+1 if n is None else n
        if n >= self.n_mats:
            n = 0
        self.curr_mat = n
        return self.annotation_sets[self.curr_mat]

    def annotate(self, contrast_limits=None):
        if self.annotation_df is None:
            self.set_annotation_round()

        GUI = AnnotationGUI(self, self.display_mode, contrast_limits)
        GUI.start()

    def annotation_to_labels(self):
        ann_dict = {}
        for basename, annset in zip(self.annotation_basenames.values, self.annotation_sets):
            ann = annset.annotation.copy()
            ann = ann.astype(np.int32) -1 # unlabeled: -1
            ann_dict = {**ann_dict, basename: ann.flatten()}
        self.annotation_df = pd.DataFrame.from_dict(ann_dict)


# instance annotation
class TileAnnotation():
    def __init__(self, loader, df, **options):
        self.loader = loader
        self.df = df
        self.df.reset_index(drop=True, inplace=True)
        self.annotation_df = None
        self.crop_size = None
        self.filename_col = None
        self.display_mode = None
        self.id_col = 'filename'
        self.track = 'obj_id'
        self.set_options(**options)
    
    def set_options(self, track=None, crop_size=None, filename_col=None, id_col=None, display_mode=None):
        self.track = track or self.track
        self.crop_size = crop_size or self.crop_size
        self.filename_col = filename_col or self.filename_col
        self.id_col = id_col or self.id_col
        self.display_mode = display_mode or self.display_mode
        
    def set_annotation_round(self):
        # annotation rounds always in 10*10 tiles
        n_tiles = self.df.shape[0]
        self.n_mats = n_tiles//100 if n_tiles%100 == 0 else n_tiles//100 +1
        self.annotation_df = self.df.loc[:,[self.filename_col, self.id_col]]
        self.annotation_df['mat'] = np.arange(n_tiles) // 100
        # self.annotation_df['label'] = np.nan
        self.annotation_sets = []
        for mat_id in tqdm(range(self.n_mats)):
            roilist = self._roilist(self.annotation_df[self.annotation_df['mat']==mat_id], 
                                    track=self.track, filename_col=self.filename_col, id_col=self.id_col)
            channel = roimat(roilist, layer='channel', crop_size=self.crop_size)
            mask = roimat(roilist, layer='mask', crop_size=self.crop_size)
            ann = np.zeros_like(channel)
            self.annotation_sets.append(AnnotationSet(mat_id=mat_id,
                                                      channel=channel, 
                                                      mask=mask, 
                                                      annotation=ann))
        self.curr_mat = -1

    def get_annotation_set(self, n=None):
        n = self.curr_mat+1 if n is None else n
        if n >= self.n_mats:
            n = 0
        self.curr_mat = n
        return self.annotation_sets[self.curr_mat]

    def annotate(self, contrast_limits=None):
        if self.annotation_df is None:
            self.set_annotation_round()

        GUI = AnnotationGUI(self, self.display_mode, contrast_limits)
        GUI.start()
    
    def annotation_to_labels(self):
        for ann in self.annotation_sets:
            labels_dict = ann.get_idxs()
            mat_id = ann.mat_id
            abs_idxs = self.annotation_df[self.annotation_df['mat'] == mat_id].index.to_list()

            for lab, idxs in labels_dict.items():
                values = np.zeros(len(abs_idxs))
                values[idxs] = lab
                self.annotation_df.loc[abs_idxs, '_'.join(['label',str(lab)])] = values

    def _roilist(self, df, filename_col='filename', id_col='obj_id', track=None):
        # retrieve rois from dataframe info
        roi_list = []
        groups = df.groupby(filename_col)
        for filename, group in tqdm(groups):
            img = self.loader.get_image(filename)
            img.crop_size = self.crop_size
            rois = img.get_roi(group[id_col].values, track=track, square=True)
            rois = [rois] if isinstance(rois, CropRoi) else rois
            roi_list.extend(rois)
        return roi_list
        

class AnnotationSet():
    def __init__(self, mat_id, channel, mask, annotation):
        self.mat_id = mat_id
        self.channel = channel
        self.mask = mask
        self.annotation = annotation.astype(np.uint8)
        self.crop_size = int(self.channel.shape[1]/10)
    
    def get_idxs(self):
        lbl, n_lbl = ndi.label(self.annotation)
        coords = ndi.center_of_mass(input= self.annotation, labels=lbl, index=np.arange(1,n_lbl+1)) # exclude 0, background
        idxs = []
        labels = []
        for y,x in coords:
            idxs.append( (x//self.crop_size) + (y//self.crop_size*10) )
            labels.append(self.annotation[int(y), int(x)])
        idxs = np.array(idxs, dtype=int)
        labels = np.array(labels, dtype=int)
        labels_dict = {}
        for lab in np.unique(labels):
            labels_dict[lab] = idxs[np.where(labels == lab)]
        return labels_dict


class AnnotationGUI():
    def __init__(self, annotation, display_mode=None, contrast_limits=None):
        self.viewer = None
        self.annotation = annotation
        self.current_set = None
        self.display_mode = display_mode
        self.contrast_limits = contrast_limits

    def clear_layers(self):
        while len(self.viewer.layers) != 0:
            self.viewer.layers.pop(0)
            
    def load_annotation_image(self):
        self.clear_layers()
        self.current_set = self.annotation.get_annotation_set()
        if self.contrast_limits is None:
            self.contrast_limits = (self.current_set.channel.min(), self.current_set.channel.max()) 
        if self.display_mode == 'channel':
            self.viewer.add_image(self.current_set.channel, name='channel - {}'.format(self.current_set.mat_id))
            self.viewer.layers['channel - {}'.format(self.current_set.mat_id)].contrast_limits = self.contrast_limits
        elif self.display_mode == 'mask':
            self.viewer.add_image(self.current_set.mask, name='mask - {}'.format(self.current_set.mat_id), opacity=0.2, colormap='green', blending='additive')
        elif self.display_mode == 'full':
            self.viewer.add_image(self.current_set.channel, name='channel - {}'.format(self.current_set.mat_id))
            self.viewer.layers['channel - {}'.format(self.current_set.mat_id)].contrast_limits = self.contrast_limits
            self.viewer.add_image(self.current_set.mask, name='mask - {}'.format(self.current_set.mat_id), opacity=0.2, colormap='green', blending='additive')
        else:
            self.viewer.add_image(self.current_set.channel, name='channel - {}'.format(self.current_set.mat_id))
            self.viewer.layers['channel - {}'.format(self.current_set.mat_id)].contrast_limits = self.contrast_limits
            self.viewer.add_image(self.current_set.mask, name='mask - {}'.format(self.current_set.mat_id), opacity=0.5, colormap='green')

        self.viewer.add_labels(self.current_set.annotation, name='annotation', opacity=.6)
        self.viewer.layers['annotation'].brush_size=30
        
    def save_annotation(self):
        # even though there is an explicit function, 
        # because we pass directly the annotation set data, and not a copy
        # the data is updated in real time anyway
        annotation = self.viewer.layers['annotation'].data
        self.current_set.annotation = annotation
        self.annotation.annotation_sets[self.annotation.curr_mat] = self.current_set
    
    def start(self):
        
        # INITIALIZE VIEWER
        self.viewer = napari.Viewer()
        self.load_annotation_image()

        # add buttons
        # a lambda function is used to pass also the viewer as argument
        buttonNext = QPushButton("NEXT")
        buttonNext.clicked.connect(lambda x: self.load_annotation_image())
        buttonSave = QPushButton("SAVE ANNOTATION")
        buttonSave.clicked.connect(lambda x: self.save_annotation())

        @napari.Viewer.bind_key('z', overwrite=True)
        def zoom_mode(v):
            if 'annotation' in v.layers:
                v.layers['annotation'].mode = 'PAN_ZOOM'
            yield
        
        @napari.Viewer.bind_key('x', overwrite=True)
        def paint_mode(v):
            if 'annotation' in v.layers:
                v.layers['annotation'].mode = 'PAINT'
            yield

        # add buttons to the gui
        self.viewer.window.add_dock_widget(buttonNext, area='left')
        self.viewer.window.add_dock_widget(buttonSave, area='left')