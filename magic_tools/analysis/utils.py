import cv2
import numpy as np
from matplotlib import get_backend
import matplotlib.pyplot as plt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from matplotlib.widgets import PolygonSelector
import seaborn as sns


def reorder_axis(image, current_order, reference_order=None):
    reference_order = 'TZCXY' if reference_order is None else reference_order

    ref_subset = ''.join([a for a in reference_order if a in current_order])
    order_dict = {a:n for (a,n) in zip(ref_subset, range(len(ref_subset)))}
    destinations = [order_dict[a] for a in current_order]

    image = np.moveaxis(image, np.arange(len(image.shape)), destinations)
    return image

    
def resize_mask(mask, factor):
    '''Resize mask, enlarge or shrink by a number of pixels (factor).
    
    Parameters
    ----------
    
    mask: np.array or CropRoi of the mask
    
    factor: int, number of pixel by which to enlarge the mask or shrink (if negative)
    
    Returns
    -------
    
    out: np.array of the resized mask'''

    if not isinstance(mask , np.ndarray):
        mask = mask.mask
    if factor >= 0:
        #edt = ndi.distance_transform_edt(np.logical_not(mask))
        edt = cv2.distanceTransform(np.logical_not(mask).astype(np.uint8), cv2.DIST_L2, 3)
        out = edt < factor
    elif factor < 0:
        #edt = ndi.distance_transform_edt(mask)
        edt = cv2.distanceTransform(mask.astype(np.uint8), cv2.DIST_L2, 3)
        out = edt > -factor
    
    return out


def slice_neighborhood(xy_slice, size=20, img_shape=None):
    '''Enlarge image slice to include neighborhood and clipping slice to image borders, if necessary.
    
    Parameters
    ----------
    
    xy_slice: slice, Slice to enlarge
        
    size: int, Neighborhood size
        
    img_size: tuple, Size of the image in the form (width, height)
    
    Returns
    -------
    
    neighborhood_slice: tuple, The enlarged slice'''

    height, width = img_shape
    y_sli = slice(max(0, min(xy_slice[0].start - size, height - 1)),
                  max(0, min(xy_slice[0].stop + size, height - 1)),
                  None)
    x_sli = slice(max(0, min(xy_slice[1].start - size, width - 1)),
                  max(0, min(xy_slice[1].stop + size, width - 1)),
                  None)
    return (y_sli, x_sli)


def clip_coordinates(x, y, shape, size):
    '''Clip crop coordinates to image boundaries.
    
    Parameters
    ----------
    
    x: int, x coordinate of crop center
    
    y: int y coordinate of crop center
    
    shape: tuple, (height, width) of the image
    
    size: half size of the crop edge
    
    Returns
    -------
    
    xy: tuple, x and y coordinate of crop center, clipped'''

    height, width = shape
    x = x if x-size > 0 else size
    y = y if y-size > 0 else size
    x = x if x+size < width else width-size
    y = y if y+size < height else height-size
    return int(x), int(y)


def resize_bbox(bbox, factor):
    resized = []
    for n,t in enumerate(bbox):
        resized.append(slice(t.start - factor, t.stop + factor, None))
    return tuple(resized)    

def clip_bbox(bbox, shape):
    height, width = shape
    yslice, xslice = bbox
    
    if yslice.start < 0:
        yslice = slice(yslice.start - yslice.start, yslice.stop - yslice.start)
    elif yslice.stop > height:
        d = yslice.stop - height
        yslice = slice(yslice.start - d, yslice.stop - d)
        
    if xslice.start < 0:
        xslice = slice(xslice.start - xslice.start, xslice.stop - xslice.start)
    elif xslice.stop > width:
        d = xslice.stop - width
        xslice = slice(xslice.start - d, xslice.stop - d)
        
    return (yslice, xslice)
    

def roi_resize(roi, threshold=50, param1=13, param2=200):
    #perim = (ndi.binary_dilation(roi.mask, structure=disk(1))^roi.mask).sum()
    cont, _ = cv2.findContours(roi.mask.astype(np.uint8), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    i = np.argmax([c.shape[0] for c in cont])
    perim = cv2.arcLength(cont[i], 1)

    # if mask touches border None, because roi does not contain complete object
    if (roi.mask[:,0].sum() + roi.mask[:,-1].sum() + roi.mask[0,:].sum() + roi.mask[-1,:].sum()) > 0:
        return (roi.label_id, None)

    if roi.mask.sum() < threshold:
        return (roi.label_id, np.zeros(roi.channel[roi.mask].flatten().shape))
    scaling = (perim * param1 / param2)
    shrunk = resize_mask(roi.mask, -scaling)
    enlarged = resize_mask(shrunk, scaling)
    if enlarged.sum() < 1:
        return (roi.label_id, np.zeros(roi.channel[roi.mask].flatten().shape))
    pixels = enlarged[roi.mask]

    return (roi.label_id, pixels)

class Gator():
    def __init__(self):
        self.size = 6
        self.poly = None
        self._data = None
        self._gate_name = None
        
    def select(self, data, gate_name, xcol, ycol, xlim=None, ylim=None, size=None):
        if size is not None:
            self.size = size
        self._data=data
        self._gate_name = gate_name
        self._xcol = xcol
        self._ycol = ycol
        self._fig, self._ax = plt.subplots(1,1,figsize=(self.size, self.size))
        self._fig.subplots_adjust(hspace=0.3)
        
        self._fig.canvas.mpl_connect("close_event", self.apply_gate)
        self.poly = PolygonSelector(self._ax, lambda x: x)

        bk = get_backend()
        if bk != 'nbAgg':
            get_ipython().run_line_magic('matplotlib', 'notebook')
        
        sns.scatterplot(data=data, x=xcol, y=ycol,
                        marker='.', linewidth=0, alpha=.2,
                        ax=self._ax)
        
        if xlim is not None:
            self._ax.set_xlim(*xlim)
        if ylim is not None:
            self._ax.set_ylim(*ylim)
        
    def apply_gate(self, event):
        polygon = Polygon(self.poly.verts)
        points = [Point(p) for p in self._data[[self._xcol,self._ycol]].values]
        sel = [polygon.contains(p) for p in points]
        self._data.loc[:,self._gate_name] = sel
        get_ipython().run_line_magic('matplotlib', 'inline')