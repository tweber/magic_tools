import pickle
import numpy as np
import pandas as pd
from multiprocessing import Pool, Manager, cpu_count
import functools
import time

import cv2
import mahotas
import skimage as ski
from scipy import stats

from skimage.morphology import disk
from scipy.ndimage import gaussian_filter, gaussian_laplace
from skimage.feature import hessian_matrix, hessian_matrix_eigvals
from skimage.filters.rank import gradient

from .image import CropRoi, ImageSet
from .utils import clip_bbox, resize_bbox, roi_resize
from .curvature import smoother, resample_tau, curvature_angles, scale_coordinates
from ..ml.classify import Classifier


###################
#   W O R K E R   #
###################

class Worker():
    def __init__(self, n_cpus=1, features_dict=None, pool=None):
        '''Abstract Worker class to perform modular image analysis jobs, with multiprocessing.
        
        Parameters
        ----------

        n_cpus: int, for the number of cpus to assign to the multiprocessing Pool

        features_dict: dict, containing the features and options for the worker job

        pool: multiprocessing.Pool shared among workers, if needed
        '''
        if n_cpus == -1:
            n_cpus = cpu_count()
        self.n_cpus = n_cpus
        self.pool = pool if pool is not None else self.initialize_pool()
        self.features_dict = features_dict
    
    def initialize_pool(self):
        '''Start the pool of workers.'''
        return Pool(self.n_cpus)

    def close_pools(self):
        '''Close and join the pool of workers.'''
        self.pool.close()
        self.pool.join()

    def run(self):
        '''Run the image processing job.'''
        raise NotImplementedError()


class WorkerCollection():
    def __init__(self, n_cpus, collection_dict):
        '''Collection of Workers that share a multiprocessing pool and, for example, can be used together in a pipeline.

        Parameters
        ----------

        n_cpus: int, for the number of cpus to assign to the multiprocessing Pool

        collection_dict: dict, containing the features_dict for each worker, worker abbreviations 
                         are available as WorkerCollection._workers_dict
        '''
        self.workers = {}
        self._workers_dict = {
            'pixext': PixelFeaturesWorker,
            'objext': ObjectFeaturesWorker,
            'findn': FindNeighbors,
            'enlarger': RoiEnlarger,
            'watershed': Watershed,
            'filter': Filter,
            'clf': Classifier
        }
        self.pool = None
        if n_cpus is not None:
            self.pool = Pool(n_cpus)
        for ext_type, features_dict in collection_dict.items():
            if all([k not in ext_type for k in self._workers_dict]):
                raise ValueError('invalid extractor type: {}'.format(ext_type))
            if 'clf' in ext_type:
                #FIXME classifier class as in classify.py, not xgboost classifier
                worker = pickle.load(open(features_dict, 'rb'))
            else:
                worker = self._workers_dict[ext_type.split('_')[0]](n_cpus, features_dict, self.pool)
            # n=0
            # while ext_type in self.workers.keys():
            #     ext_type = ext_type+'_{}'.format(n)
            #     n +=1
            self.workers[ext_type] = worker
            setattr(self, ext_type, worker)

    def get_workers(self):
        return self.workers.values()


#####################################
#   O B J E C T   F E A T U R E S   #
#####################################

class ObjectFeaturesWorker(Worker):
    def __init__(self, n_cpus=1, features_dict=None, pool=None, **kwargs):
        '''Worker class for object features. It leverages multithreading to compute object features at speed.
        Basic, convex hull, intensity, shape, texture and curvature features are supported.
        
        Parameters
        ----------
        
        n_cpus: int, number of available cpus. If -1 it uses all available
        
        features_dict: dict, dictionary describing which features to calculate
        
        pool: multiprocessing.Pool shared among workers, if needed
        '''
        
        features_dict = {k: True for k in ['basic', 'chull', 'intensity', 'shape', 'texture', 'curvature']} if features_dict is None else features_dict
        super().__init__(n_cpus=n_cpus, features_dict=features_dict, pool=pool)
        

    def run(self, image, label_id=None, track=None, channel=None, label=None, segmentation=None, neighborhood=20):
        '''Extract features from specified, labeled objects in an image, as ImageSet.
        ImageSet requires a channel, segmentation and labels for feature extraction, which can also be defined as a track.
        Rois are generated from the image, based on label_ids and features are computed in parallel, roi by roi.
        
        Parameters
        ----------
        
        image: ImageSet, containing channel, segmentation and labels on which to calculate features
        
        label_id: list of int, list of labels for the objects for which to calculate features. If None all labels are considered
        
        track: str, name of the track defining channel, segmentation and labels
        
        chanel: str, name of the channel as in the ImageSet.layers
        
        label: str, name of the layer as in the ImageSet.layers
        
        segmentation: str, name of the segmentation as in the ImageSet.layers
        
        neighborhood: int, size of the neighborhood for neighborhood intensity feature calculations'''

        label_id = [label_id] if isinstance(label_id, int) else label_id
        
        # get rois
        rois = None
        if isinstance(image, ImageSet):
            rois = image.get_roi(label_id, track=track, channel=channel, label=label, segmentation=segmentation, neighborhood=neighborhood)
        elif isinstance(image, Image):
            rois = image.get_roi(label_id)

        if isinstance(rois, CropRoi):
            rois = [rois]
        if len(rois) == 0:
            return pd.DataFrame()

        # set up multiprocessing
        with Manager() as manager:
            manager = Manager()
            out_list = manager.list()

            rois = [rois] if isinstance(rois, CropRoi) else rois
            fts = list(self.features_dict.values())
            args = [(roi, *fts, out_list) for roi in rois]
            
            # run feature extraction
            t0 = time.time()
            _ = self.pool.map(calculate_object_features_multiprocessing, args)
            tf = time.time()
            
            print('compute fts on {}: {:.2f}'.format(track, tf - t0))
            
            df = pd.DataFrame(list(out_list))
            df['filename'] = image.name
        return df

    
def calculate_object_features_multiprocessing(arg_list):
    '''Utility function for multiprocessing object features calculation'''
    croproi, basic, chull, intensity, shape, texture, curvature, out_list = arg_list
    try:
        results_dict = calculate_object_features(croproi, basic, chull, intensity, shape, texture, curvature)
        if results_dict:
            out_list.append(results_dict)
    except:
        print('FAILED:   image {} --- label {}'.format(croproi.name, croproi.label_id))

def calc_basic_features(croproi):
    '''Calculate basic roi features:
    - object id
    - x,y centroid coordinates
    - area
    - perimeter'''

    # AREA AND PERIMETER
    area = croproi.mask.sum()
    #edge = croproi.mask ^ ndi.binary_erosion(croproi.mask, structure=disk(1))
    #perimeter = edge.sum()
    cont, _ = cv2.findContours(croproi.mask.astype(np.uint8), cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
    cont = cont[np.argmax([len(c) for c in cont])] # get largest contour
    perimeter = cv2.arcLength(cont, 1)
    results_dict = {
        'obj_id': croproi.label_id,
        'X': croproi.coordinates[1],
        'Y': croproi.coordinates[0],
        'area': area,
        'perimeter': perimeter}
    return results_dict

def calc_chull_features(croproi):
    '''Calculate convex hull roi features:
    - convex hull area
    - convex hull perimeter
    '''
    # CONVEX HULL AREA AND PERIMETER
    cont, _ = cv2.findContours(croproi.mask.astype(np.uint8), cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
    cont = cont[np.argmax([len(c) for c in cont])] # get largest contour
    chull = cv2.convexHull(cont)
    perimeter_chull = cv2.arcLength(chull, 1)
    area_chull = cv2.contourArea(chull)
    results_dict = {
        'area_chull': area_chull,
        'perimeter_chull': perimeter_chull
    }
    return results_dict

def calc_intensity_features(croproi, ng=False):
    '''Calculate roi intensity feature, with optional neighborhood intensity features.
    - histogram of intensity (and normalized)
    - intensity distribution properties (min, max, mean, variance, skew, kurtosis)
    - total intensity'''

    # INTENSITY
    # intensity distribution properties
    _, minmax, mean, var, skew, kurt = stats.describe(croproi.pixels())    
    # calculate histogram intensities
    hist_raw = np.histogram(croproi.pixels(), bins=64, density=False, range=(0, 255))
    # include range min-max, ilastik style
    hist = np.histogram(croproi.pixels(), bins=64, density=False, range=(minmax[0], minmax[1]))
    # total intensity
    tot_int = croproi.pixels().sum()
    result_dict = {
        **{'hist_raw_' + str(i): hist_bin for i, hist_bin in enumerate(hist_raw[0])},
        **{'hist_' + str(i): hist_bin for i, hist_bin in enumerate(hist[0])},
        'min_int': minmax[0],
        'max_int': minmax[1],
        'mean_int': mean,
        'var_int': var,
        'skew_int': skew,
        'kurt_int': kurt,
        'tot_int': tot_int,
    }
    # FIXME adjust dict keys dynamically
    if ng:
        result_dict = {
        **{'hist_raw_ng' + str(i): hist_bin for i, hist_bin in enumerate(hist_raw[0])},
        **{'hist_ng' + str(i): hist_bin for i, hist_bin in enumerate(hist[0])},
        'min_int_ng': minmax[0],
        'max_int_ng': minmax[1],
        'mean_int_ng': mean,
        'var_int_ng': var,
        'skew_int_ng': skew,
        'kurt_int_ng': kurt,
        'tot_int_ng': tot_int,
    }
    return result_dict

def calc_intensity_features_ng(croproi, croproing):
    '''Calculate intensity features of the neighborhood as in calc_intensity_features().
    Addionally, calculate ratio of total intensity of roi and its neighborhood.'''

    intden = calc_intensity_features(croproi)
    intdenng = calc_intensity_features(croproing, True)
    tot_int = intden['tot_int']
    tot_int_ng = intdenng['tot_int_ng']
    tot_int_ratio = tot_int / tot_int_ng if tot_int_ng > 0 else np.nan
    result_dict = {**intden, **intdenng, 'tot_int_ratio': tot_int_ratio}
    return result_dict

def calc_shape_features(croproi, basic_features, chull_features):
    '''Calculate roi shape features. It requires also basic_features and convex_hull features for computation.
    - principal components
    - roundness, aspect_ratio, area/perimeter, circularity, elongation
    - convexity of area and perimeter
    - thinnes ratio, contour temperature
    - zernike moments, hu moments calculated on roi mask'''

    area = basic_features['area']
    perimeter = basic_features['perimeter']
    area_chull = chull_features['area_chull']
    perimeter_chull = chull_features['perimeter_chull']

    # PCA
    means, eVecs, eVals = cv2.PCACompute2((croproi.mask).astype('uint8'), None)
    long_axis = eVals[0][0]
    short_axis = eVals[1][0]

    # simple shape descriptors
    roundness = mahotas.features.roundness(croproi.mask)
    aspect_ratio = long_axis / short_axis
    area_perimeter_ratio = area / perimeter
    circularity = np.power(perimeter, 2) / area
    elongation = 1 - (short_axis / long_axis)
    convexity_area = area_chull / area
    convexity_perimeter = perimeter_chull / perimeter
    thinnes_ratio = 4 * np.pi * (area / np.power(perimeter, 2))
    contour_temperature = np.power(np.log2((2 * perimeter) * (perimeter - perimeter_chull)), -1) if (2 * perimeter) * (
                perimeter - perimeter_chull) > 0 else 0

    # hu moments
    moms = cv2.moments(((croproi.mask) * 255).astype('float32'))
    hu_moms_mask = cv2.HuMoments(moms).flatten()

    # zernike moments
    ze_moms_mask = mahotas.features.zernike_moments(croproi.mask, np.max(croproi.mask.shape), degree=8)
    
    result_dict = {
        'pca_long_axis': long_axis,
        'pca_short_axis': short_axis,
        'roundness': roundness,
        'aspect_ratio': aspect_ratio,
        'area_perimeter_ratio': area_perimeter_ratio,
        'circularity': circularity,
        'elongation': elongation,
        'convexity_area': convexity_area,
        'convexity_perimeter': convexity_perimeter,
        'thinnes_ratio': thinnes_ratio,
        'contour_temperature': contour_temperature,
        **{'hu_mask_' + str(k): hu_mom for k, hu_mom in enumerate(hu_moms_mask)},
        **{'ze_mask_' + str(l): ze_mom for l, ze_mom in enumerate(ze_moms_mask[:12])}
    }
    
    return result_dict

def calc_texture_features(croproi):
    '''Calculate roi texture features.
    - parameter-free threshold adjacency statistics
    - zernike moments, hu moments calculated on pixel intensities'''

    pftas = mahotas.features.pftas(croproi.zero_out())

    # hu moments on filtered image
    moms = cv2.moments(croproi.zero_out().astype('float32'))
    hu_moms_img = cv2.HuMoments(moms).flatten()

    # zernike moments on filtered image
    ze_moms_img = mahotas.features.zernike_moments(croproi.zero_out(), np.max(croproi.zero_out().shape), degree=8)

    result_dict = {
        **{'hu_img_' + str(k): hu_mom for k, hu_mom in enumerate(hu_moms_img)},
        **{'ze_img_' + str(l): ze_mom for l, ze_mom in enumerate(ze_moms_img[:12])},
        **{'pftas_' + str(m): ta for m, ta in enumerate(pftas)},
    }
    return result_dict

def calc_curvature_features(croproi):
    '''Calculate roi curvature features.
    - histogram of curvatures between 150 an 210 degrees'''

    #contour = ski.measure.find_contours(croproi.mask, .5)[0]
    cont, hier = cv2.findContours(croproi.mask.astype(np.uint8), cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
    contour = cont[0]
    contour = contour.reshape((contour.shape[0], 2))

    smoothed_contour = smoother(contour,40) # smoothening contour
    resampled_contour = resample_tau(smoothed_contour, 128, tau=0.21, hat_support=100) # resampling contour
    curvature = curvature_angles(scale_coordinates(resampled_contour)) # calculate curvatures
    hist_curv = np.histogram(curvature, bins=32, density=False, range=(150, 210))

    result_dict = {
        **{'hist_curv' + str(j): hist_curv for j, hist_curv in enumerate(hist_curv[0])}
    }
    return result_dict

def calculate_object_features(croproi, basic=True, chull=True, intensity=True, shape=True, texture=True, curvature=True):
    '''Calculate features of a CropRoi instance.
    Features span basic, convex hull, intensity, shape, texture and curvature.
    - object id
    - x,y centroid coordinates
    - area
    - perimeter
    - convex hull area
    - convex hull perimeter
    - histogram of intensity (and normalized)
    - intensity distribution properties (min, max, mean, variance, skew, kurtosis)
    - total intensity (of neighborhood)
    - principal components
    - roundness, aspect_ratio, area/perimeter, circularity, elongation
    - convexity of area and perimeter
    - thinnes ratio, contour temperature
    - zernike moments, hu moments calculated on roi mask
    - parameter-free threshold adjacency statistics
    - zernike moments, hu moments calculated on pixel intensities
    - histogram of curvatures between 150 an 210 degrees

    Parameters
    ----------
    
    croproi: CropRoi, of the object
    
    basic, chull, intensity, shape, texture, curvature: bool, if True a specific feature panel is calculated
    
    Returns
    -------
    
    result_dict: dict, containing all calculated features
    '''
    fts = []
    
    if basic:
        fts.append(calc_basic_features(croproi))

    if fts[0]['area'] < 20:
        return False

    if chull:
        fts.append(calc_chull_features(croproi))
    
    if intensity:
        # NEIGHBORHOOD ROI
        # makes sense to calculate only if the area-filter is passed
        croproing = croproi.copy()
        #croproing.mask = np.copy(ndi.binary_dilation(croproing.mask, structure=disk(croproing.neighborhood)) ^ croproing.mask)
        croproing.mask = np.copy(cv2.dilate(croproing.mask.astype(np.uint8), kernel=disk(croproing.neighborhood), iterations=1).astype(bool) ^ croproing.mask)

        fts.append(calc_intensity_features_ng(croproi, croproing))
    
    if shape:
        fts.append(calc_shape_features(croproi, fts[0], fts[1]))
    
    if texture:
        fts.append(calc_texture_features(croproi))
    
    if curvature:
        fts.append(calc_curvature_features(croproi))
    
    result_dict = {}
    for ft in fts:
        result_dict = {**result_dict, **ft}
    return result_dict
    

###################################
#   P I X E L   F E A T U R E S   #
###################################

class PixelFeaturesWorker(Worker):
    def __init__(self, n_cpus, features_dict=None, pool=None, **kwargs):
        '''Worker class for pixel features. It leverages multithreading to compute pixel features at speed.
        Each feature is calculated as follows: 
        a gaussian filter is first applied, then a second filter such as gradient, hessian or laplacian.

        Parameters
        ----------
        
        n_cpus: int, number of available cpus. If -1 it uses all available
        
        features_dict: dict, dictionary describing which features to calculate.
                       keys are filters ('gaussian', 'gradient', 'hessian', 'laplacian')
                       values are list of structure sizes for the filters
        
        pool: multiprocessing.Pool shared among workers, if needed
        '''
        
        super().__init__(n_cpus=n_cpus, features_dict=features_dict, pool=pool)
        self.filters = { 'gaussian' : gaussian_filter_args,
                         'gradient' : gradient_args,
                         'hessian'  : calc_hessian_args,
                         'laplacian': gaussian_laplace_args}

    def run(self, image, channel, dataframe=False):
        '''Extract pixel features from specified channel of an image, as ImageSet.
        ImageSet requires a channel.
        In the first round, gaussian filters are applied, then for each gaussian secondary filters are applied.
        Pixel features are computed in parallel for each round.
        
        Parameters
        ----------
        
        image: ImageSet, containing channel on which to calculate pixel features
        
        chanel: str, name of the channel as in the ImageSet.layers
        
        dataframe: bool, outputs a dataframe instead of a list of images
        
        Returns
        -------
        
        output: list of numpy.ndarray or pandas.DataFrame, list of filtered images or dataframe with pixel values'''

        img = image.channels[channel]

        if self.features_dict is None:
            raise RuntimeError('Features dictionary is not set up.')
        
        with Manager() as manager:
            gau_list = manager.list()

            # first round: gaussians

            # parse arguments
            t0 = time.time()
            gau_args = []

            fts_dict = self.features_dict.copy()
            sigmas = fts_dict.pop('gaussian')
            # preparing functions and arguments to execute with partials
            for s in sigmas:
                gau_args.append(functools.partial(_launcher, [self.filters['gaussian'], 'gaussian', img.copy(), s, gau_list]))
                #gau_args.append((self.filters['gaussian'], 'gaussian', img.copy(), s, gau_list))
                        
            # run feature extraction
            _ = self.pool.map(_dispatcher, gau_args)
            
            gaussians = list(gau_list)

            # second round: filters on gaussians
            filt_list = manager.list()
            filt_args = []
            # gaussian loop
            for gau, gau_img in gaussians:
                # secondary filter loop
                for filt, sigmas in fts_dict.items():
                    # preparing functions and arguments to execute with partials
                    for s in sigmas:
                        #filt_args.append((self.filters[filt], gau+'_'+filt, gau_img.copy(), s, filt_list))
                        filt_args.append(functools.partial(_launcher, [self.filters[filt], gau+'_'+filt, gau_img.copy(), s, filt_list]))
            
            # run feature extraction
            _ = self.pool.map(_dispatcher, filt_args)
            
            # extract output
            output = list(filt_list)
            output.extend(gaussians)
            tf = time.time()
            print('compute pixel features: {:.2f}'.format(tf - t0))

            # output dataframe
            if dataframe:
                df = {}
                for feature, values in output:
                    if 'hessian' in feature:
                        df[feature+'_0'] = values[0].flatten()
                        df[feature+'_1'] = values[1].flatten()
                    else:
                        df[feature] = values.flatten()
                df = pd.DataFrame.from_dict(df)
                df['filename'] = image.name
                return df
            else:
                return output
            

def calc_hessian(img, s):
    '''Utility function for multiprocessing'''
    H_elems = hessian_matrix(img, sigma=s, order='rc')
    hme = hessian_matrix_eigvals(H_elems)
    return hme

def gaussian_filter_args(args):
    '''Utility function for multiprocessing'''
    return cv2.GaussianBlur(args[0].astype(np.uint8), ksize=(9,9), sigmaX=args[1], borderType=cv2.BORDER_DEFAULT)
    #return gaussian_filter(args[0], float(args[1]))

def gradient_args(args):
    '''Utility function for multiprocessing'''
    return cv2.morphologyEx(args[0].astype(np.uint8), cv2.MORPH_GRADIENT, disk(float(args[1])))
    #return gradient(args[0], disk(float(args[1])))

def calc_hessian_args(args):
    '''Utility function for multiprocessing'''
    return calc_hessian(args[0], float(args[1]))

def gaussian_laplace_args(args):
    '''Utility function for multiprocessing'''
    return cv2.Laplacian(args[0].astype(np.uint8), ksize=float(args[1]))
    #return gaussian_laplace(args[0], float(args[1]))

def _launcher(args):
    '''Utility function for multiprocessing. 
    The launcher executes the function with certain arguments and appends the result to a Manager.list().'''

    fun, filt, img, s, out_list = args
    out = fun([img, s])
    out_list.append((f'{filt}_{s}', out))
    return

def _dispatcher(f):
    '''Utility function for multiprocessing. 
    The dispactcher does nothing else than complete execution of the partials.'''
    return f()


#########################
#   W A T E R S H E D   #
#########################

class Watershed(Worker):
    def __init__(self, n_cpus=None, features_dict=None, pool=None):
        super().__init__(n_cpus=n_cpus, features_dict=features_dict, pool=pool)
        
        self.features_dict = {
            'area': [],
            'perimeter': [],
            'min_int': [],
            'max_int': [],
            'mean_int': [],
            'var_int': [],
            'skew_int': [],
            'kurt_int': [],
            'tot_int': []}
        
        if features_dict is not None:
            for k,v in features_dict.items():
                self.features_dict[k] = v
        
        self.objext = ObjectFeaturesWorker(self.n_cpus, {'basic':True, 'chull':False, 'intensity':False, 'shape':False, 'texture':False, 'curvature':False}, pool=self.pool)

    def run(self, image, track=None, channel=None, label=None, segmentation=None, multiprocessing=True):
        
        # collect features
        fts_df = self.objext.run(image, channel=channel, segmentation=segmentation, label=label, track=track)        
        
        # parse arguments
        if track is not None:
            label = image.tracks.loc[image.tracks['track'] == track, 'label'].iloc[0]
            channel = image.tracks.loc[image.tracks['track'] == track, 'channel'].iloc[0]
        
        lbls = image.labels[label].copy()
        img = image.channels[channel].astype(np.uint8)
        
        # filter objects to watershed
        if len(fts_df) == 0:
            return lbls.copy()
        to_watershed = self.filter_objects(fts_df)['obj_id'].to_list()
        if len(to_watershed) == 0:
            return lbls.copy()

        mask = np.isin(image.labels[label], to_watershed)

        t0 = time.time()
        if multiprocessing is True:
            rois = image.get_roi(to_watershed, channel=channel, label=label)
            if isinstance(rois, CropRoi):
                rois = [rois]
            out = self.pool.map(watershed_croproi, rois)

            maxl = lbls.max()
            for label_id, roiwat in out:
                pos = roiwat>0 # select positive values (not-zeros)
                roiwat[pos] = roiwat[pos] + maxl # shift pixels if positive 
                maxl = roiwat.max() # progressively increase label_id with the new masks
                lbls[lbls == label_id] = roiwat  # update label_ids
            
        elif multiprocessing is False:
            out = watershed(img, mask)
            maxl = lbls.max()
            out[out>0] = out[out>0] + maxl # shift labels
            lbls[mask>0] = out[mask>0] # substitute labels
            
        tf = time.time()    
        print('WATERSHED time: {:.2f}'.format(tf-t0))    
        return lbls

    def filter_objects(self, df):
        subdf = df.copy()
        for label, interval in self.features_dict.items():
            if len(interval) == 0:
                continue
            subdf = subdf[subdf[label].between(*interval)]
        return subdf


def watershed_croproi(croproi):
    mask = croproi.mask.astype(np.uint8)
    image = croproi.channel.astype(np.uint8)
    out = watershed(image, mask)
    pixels = out[mask>0]

    return (croproi.label_id, pixels)

def watershed(image, mask, r=10, ng=51):
    # find watershed markers
    # distance transform
    selem = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (r,r)) # r = radius of disk
    dist = cv2.distanceTransform((mask).astype(np.uint8), cv2.DIST_L2, 5)
    # morphological open, because otherwise, max filter will omit peaks
    dist = cv2.dilate(dist.astype(np.uint8), kernel=selem)
    dist = cv2.erode(dist.astype(np.uint8), kernel=selem)
    # max filtering
    neighborhood = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (ng,ng)) # radius of local max neighborhood
    local_max = cv2.dilate(dist.astype(np.uint8), kernel=neighborhood, iterations=1) == dist
    # subtract background
    background = (dist==0)
    eroded_background = cv2.erode(background.astype(np.uint8), kernel=neighborhood)>0
    detected_maxima = local_max ^ eroded_background
    detected_maxima = cv2.dilate(detected_maxima.astype(np.uint8), kernel=selem)
    # label markers
    _, markers = cv2.connectedComponents(detected_maxima)

    # inspired by: https://doi.org/10.1038/s41598-019-38813-2
    # normalize intensity and distance map
    # generate the basin
    med = cv2.medianBlur(image, ksize=9)
    re_img = ski.exposure.rescale_intensity(med.astype('float'), out_range=(0,1))
    re_dis = ski.exposure.rescale_intensity(dist.astype('float'), out_range=(0,1))
    basin = -re_img-re_dis
    basin = ski.exposure.rescale_intensity(basin.astype('float'), out_range=(0,255))
    
    # watershed from skimage not opencv works better
    watershed = ski.segmentation.watershed(
        basin, 
        markers,
        #compactness=0.5,
        mask=mask,
        watershed_line=True)
    
    return watershed


###################################
#   F I N D   N E I G H B O R S   #
###################################

class FindNeighbors(Worker):
    def __init__(self, n_cpus, features_dict=None, pool=None, **kwargs):
        '''Worker class for nearest neighbors between masks. It leverages multithreading to compute nearest neighbors at speed.
        
        Parameters
        ----------
        
        n_cpus: int, number of available cpus. If -1 it uses all available

        features_dict: dict, specifying worker's parameters 'seeds_label', 'neighbors_label', 'search_range' and 'increment'

        pool: multiprocessing.Pool shared among workers, if needed
        '''
        self.features_dict = {
            'seeds_label': None,
            'neighbors_label': None,
            'search_range': 85,
            'increment': 1
        }
        if features_dict is not None:
            for k in self.features_dict.keys():
                if k in features_dict:
                    self.features_dict[k] = features_dict[k]
        super().__init__(n_cpus=n_cpus, features_dict=features_dict, pool=pool)
        

    def run(self, img, label_ids=None):
        '''Search for nearest neighbors of the segmented objects in seeds_label layer, among the segmented objects in the neighbors_label layer
        
        Parameters
        ----------
        
        img: ImageSet containing the seed and neighbors layers
        
        label_ids: list of int, of the ids of the seed objects
        
        Returns
        -------
        
        df: pandas dataframe with rows for each label that has a neighbor'''

        if label_ids is None:
            label_ids = img.label_idxs[self.features_dict['seeds_label']]

        if len(label_ids) == 0:
            return pd.DataFrame()

        rois = []
        for idx in list(label_ids):
            if idx not in img.labels_bboxes[self.features_dict['seeds_label']]:
                continue
            bbox = img.labels_bboxes[self.features_dict['seeds_label']][idx]
            bbox = resize_bbox(bbox, self.features_dict['search_range'])
            bbox = clip_bbox(bbox, img.shape)

            sd_lbl = img.labels[self.features_dict['seeds_label']][bbox]
            sd_mask = (sd_lbl == idx)
            ng_lbl = img.labels[self.features_dict['neighbors_label']][bbox]

            roi = CropRoi(name = img.name,
                        channel = None,
                        label = ng_lbl,
                        mask = sd_mask,
                        segmentation = None,
                        label_id = idx,
                        coordinates = None,
                        neighborhood=None)
            rois.append(roi)

        # set up multiprocessing
        with Manager() as manager:
            manager = Manager()
            out_list = manager.list()

            args = [(roi, self.features_dict['search_range'], self.features_dict['increment'], out_list) for roi in rois]
            # run feature extraction
            t0 = time.time()
            _ = self.pool.map(find_neighbor_multiprocessing, args)
            tf = time.time()
            
            print('find neighbors: {:.2f}'.format(tf - t0))
            
            df = pd.DataFrame(list(out_list))
            df['filename'] = img.name
            
        return df


def find_neighbor_multiprocessing(arg_list):
    '''Utility function for multiprocessing nearest neighbor on segmented objects'''

    croproi, search_range, increment, out_list = arg_list
    try:
        results_dict = find_neighbor(croproi, search_range, increment)
        if results_dict:
            out_list.append(results_dict)
    except:
        print('FIND NEIGHBOR FAILED:   image {} --- label {}'.format(croproi.name, croproi.label_id))

def find_neighbor(croproi, search_range=85, increment=1):
    '''Find the nearest neighbor of a mask among other labeled masks.
    
    Parameters
    ----------
    
    croproi: CropRoi with mask attribute being the seed label and label attribute containing labeled candidate neighbors.
    
    search_range: int, how far a neighbor can be located
    
    increment: int, step increase in the search range between iterations
    
    Returns
    -------
    
    dict containing the 'seed_id', 'neighbor_id' and the 'distance' between the two'''

    for i in range(search_range+1):
        neighborhood_mask = croproi.label[croproi.mask.astype(bool)]
        nearest = np.max(neighborhood_mask)
        if nearest > 0:
            nearest_neighbor = np.argmax(np.bincount(neighborhood_mask)[1:]) + 1
            break
        croproi.mask = cv2.dilate(croproi.mask.astype(np.uint8), kernel= disk(increment), iterations=1)
    
    if i > search_range:
        return None
    return {'seed_id': croproi.label_id, 'neighbor_id': nearest_neighbor, 'distance': i}


###############################
#   R O I   E N L A R G E R   #
###############################

class RoiEnlarger(Worker):
    def __init__(self, n_cpus=1, features_dict=None, pool=None, **kwargs):
        '''Smoothen rois contours by resizing them. Rois are resized by a factor calculated as following:
        factor = perimeter * param1 / param2
        Rois whose area is below a threshold are ignored.

        Parameters
        ----------
        
        img: ImageSet containing the seed and neighbors layers
        
        features_dict: dict of parameters for the worker: 'threshold', 'param1', 'param2'
        '''

        # default values are based on micronuclei classification
        self.features_dict = {
            'threshold': 50, 
            'param1': 8,
            'param2': 120}
        if features_dict is not None:
            self.features_dict = features_dict
        super().__init__(n_cpus, self.features_dict, pool=pool)
        
    def run(self, img, track):
        '''Resize rois in an ImageSet. Labels are defined in a ImageSet track.
        
        Parameters
        ----------
        
        img: ImageSet, containing labels/rois to resize
        
        track: str, name of the track containing the labels to resize
        
        Returns
        -------
        
        output: numpy.ndarray image with the resized labels'''

        label = img.tracks[img.tracks['track'] == track]['label'].values[0]

        lbl_nucl = img.labels[label].copy()

        rois = img.get_roi(track=track, neighborhood=1)
        if isinstance(rois, CropRoi):
            rois = [rois]
        if len(rois) == 0:
            return lbl_nucl

        args = [(roi, self.features_dict['threshold'], self.features_dict['param1'], self.features_dict['param2']) for roi in rois]
        out = self.pool.map(roi_resize_multiprocessing, args)

        for label_id, pixels in out:
            if pixels is not None: 
                lbl_nucl[lbl_nucl == label_id] = pixels * label_id # preserve label_id
            else:  # captures objects that are bigger than the rois 
                lbl_nucl[lbl_nucl == label_id] = 0
            
        return lbl_nucl


def roi_resize_multiprocessing(arg_list):
    croproi, threshold, param1, param2 = arg_list
    out = roi_resize(roi=croproi, threshold=threshold, param1=param1, param2=param2)
    return out
    

###################
#   F I L T E R   #
###################

class Filter(Worker):
    def __init__(self, n_cpus=None, features_dict=None, pool=None):
        super().__init__(n_cpus=n_cpus, features_dict=features_dict, pool=pool)
        
        self.features_dict = {
            'area': [],
            'perimeter': [],
            'min_int': [],
            'max_int': [],
            'mean_int': [],
            'var_int': [],
            'skew_int': [],
            'kurt_int': [],
            'tot_int': []}
        
        if features_dict is not None:
            for k,v in features_dict.items():
                self.features_dict[k] = v
        
        self.objext = ObjectFeaturesWorker(self.n_cpus, {'basic':True, 'chull':False, 'intensity':False, 'shape':False, 'texture':False, 'curvature':False}, pool=self.pool)

    def run(self, image, track=None, channel=None, label=None, segmentation=None, multiprocessing=True):
        
        # collect features
        fts_df = self.objext.run(image, channel=channel, segmentation=segmentation, label=label, track=track)        
        
        # parse arguments
        if track is not None:
            label = image.tracks.loc[image.tracks['track'] == track, 'label'].iloc[0]
            channel = image.tracks.loc[image.tracks['track'] == track, 'channel'].iloc[0]
        
        lbls = image.labels[label].copy()
        img = image.channels[channel].astype(np.uint8)
        
        # filter objects to watershed
        if len(fts_df) == 0:
            return lbls.copy()
        to_filter = self.filter_objects(fts_df)['obj_id'].to_list()
        if len(to_filter) == 0:
            return lbls.copy()

        mask = np.isin(image.labels[label], to_filter)
        filtered = image.labels[label].copy()*mask
        return filtered

    def filter_objects(self, df):
        subdf = df.copy()
        for label, interval in self.features_dict.items():
            if len(interval) == 0:
                continue
            subdf = subdf[subdf[label].between(*interval)]
        return subdf
