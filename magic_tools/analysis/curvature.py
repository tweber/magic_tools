import numpy as np
from skimage import measure
from glob import glob
from skimage.io import imread
from scipy.ndimage import label
from tqdm import tqdm


##############################
# SMOOTHENING AND RESAMPLING #
##############################

# the following functions are coming from the inshade package
# connected to the following publication: https://doi.org/10.1016/j.cag.2021.04.037

def is_equal(p1,p2, eps=1e-12):
    return np.sum((p1-p2)**2)<eps

def is_closed(contour, eps=1e-12):
    return is_equal(contour[0],contour[-1],eps)

def is_none(val):
    return type(val) == type(None)

def parameterize(contour, alpha=0.5, normalize=None):
    squared_distances = np.sum((contour[:-1]-contour[1:])**2, axis=1)
    result = np.cumsum(np.insert(squared_distances**alpha, 0, 0.0))
    if is_none(normalize):
        return result
    else:
        return result/result[-1]*normalize

def barycenter(contour):
    par = parameterize(contour)
    length = par[-1]
    par = np.repeat((par[1:] - par[:-1])[..., None], 2, axis=1)
    return np.sum(par * (contour[1:] + contour[:-1]), axis=0) / (2.0 * length)


def area(contour):
    if not is_closed(contour):
        return 0.0
    r = np.min(contour, axis=0) - np.array([2.0, 2.0])
    q = contour - r
    r = q.copy()
    r[:, [0, 1]] = r[:, [1, 0]]
    s = q[1:, :] * r[:-1, :]
    t = s[:, 0] - s[:, 1]
    return np.sum(t)


def recenter(source, target):
    center_source = barycenter(source)
    if len(target.shape) == 2:
        center_target = barycenter(target)  # compute target center
    else:
        center_target = target              # use provided target center
    return source[:, ] + center_target - center_source


def smoother(contour, iterations=1, norm=0.5):
    if iterations == 0:
        return contour.copy()
    loc_contour = contour.copy()
    bc = barycenter(loc_contour)
    closed = False
    ref_area = 0.0
    if is_closed(contour):
        closed = True
        ref_area = area(loc_contour)
        loc_contour = np.insert(loc_contour, loc_contour.shape[0], loc_contour[1, :], axis=0)
    par = parameterize(loc_contour)
    ref_len = parameterize(loc_contour, norm)[-2]
    for i in range(iterations):
        r = np.empty(loc_contour.shape)
        r[0, :], r[-1, :] = loc_contour[0, :], loc_contour[-1, :] # boundaries
        d = np.repeat((par[2:] - par[:-2])[..., None], 2, axis=1)
        q = np.repeat((par[1:] - par[:-1])[..., None], 2, axis=1)
        a = q[:-1, :]*(loc_contour[1:-1, :] + loc_contour[:-2, :])
        b = q[1:, :]*(loc_contour[2:, :] + loc_contour[1:-1, :])
        r[1:-1, :] = 0.5*(a+b)/d
        p = parameterize(r)
        if closed:
            r[0, :], r[-1, :] = r[-2, :], r[1, :]    # smoothed boundaries
        if ref_area>0.0:
            this_area = area(r[:-1, :])
            r *= np.sqrt(ref_area / this_area)
        else:
            this_len = parameterize(r, norm)[-2]
            r *= ref_len / this_len
        recenter(r, bc)
        loc_contour = r
    if not closed:
        return recenter(loc_contour, bc)
    else:
        loc_contour[0, :] = loc_contour[-2, :]    # move smoothed C[0] from back to front
        return recenter(loc_contour[:-1, :], bc)


def __circle_intersect(center, p1, p2, ref_dir):
    dc, di = center - p1, p2 - p1
    a, b, c = np.dot(di, di), -2.0 * np.dot(dc, di), np.dot(dc, dc) - 1.0
    if a == 0.0: return np.array([])
    disc = b ** 2 - 4 * a * c
    if disc < 0: return np.array([])
    a1 = (-b + np.sqrt(disc)) / (2.0 * a)
    a2 = (-b - np.sqrt(disc)) / (2.0 * a)
    # pick alpha in [0,1] aligned with last ref_dir
    if a1 > a2: a1, a2 = a2, a1
    score = -1e99
    if 0 <= a1 <= 1:
        p = p1 + a1 * (p2 - p1)
        score = np.dot(p - center, ref_dir)
    if 0 <= a2 <= 1:
        q = p1 + a2 * (p2 - p1)
        return p if score > np.dot(q - center, ref_dir) else q
    return np.array([])


def __reparam(contour, rescale):
    result = [contour[0]]
    last_p = np.array(result[-1])
    n=1
    ref_dir = np.zeros(2)
    while n<len(contour):
        p = __circle_intersect(last_p, contour[n-1, :], contour[n, :], ref_dir)
        if p.size == 2:
            ref_dir = p - last_p
            result.append(p)
            last_p = p
        else:
            n += 1
    if is_closed(contour) and rescale:
        result += [result[0]]   # close contour without additional conditions
    else:
        if not is_equal(result[-1], contour[-1]):   # place last point if necessary
            result += [contour[-1]]
    return np.array(result)


def reparameterize(contour, rescale=True):
    if not rescale: return __reparam(contour, False), 1.0
    iterations = 25
    error = 10 ** 10
    loc_contour = contour.copy()
    scale = 1.0
    while iterations > 0 and error > 1e-6:
        loc_contour = __reparam(loc_contour, True)
        t = parameterize(loc_contour)
        loc_contour *= np.ceil(t[-1]) / t[-1]
        scale *= np.ceil(t[-1]) / t[-1]
        error = np.ceil(t[-1]) - t[-1]
        iterations -= 1
    return recenter(loc_contour, contour), scale


def neg_ramp(x,xl,xr):
    if abs(xr-xl)<1.0e-8: return 0.0
    return  1.0-(x-xl)/(xr-xl) if xl <= x <= xr else 0.0
        

def ramp(x,xl,xr):
    if abs(xr-xl)<1.0e-8: return 0.0
    return  (x-xl)/(xr-xl) if xl <= x <= xr else 0.0
    

def hat(x,xl,xc,xr):
    if abs(x-xc)<1.0e-8: 
        res = 1.0
    else:
        res = 0.0
        if xl < x < xr:
            res = ramp(x,xl,xc) if x<xc else neg_ramp(x,xc,xr)
    return res


def uniform(C,nsamples,hat_support=3):
    T  = parameterize(C)
    dT = T[-1]
    C_unif = np.zeros((nsamples,C.shape[1]))
    C_unif[0] = C[0]
    o_to_s = C.shape[0]/nsamples 
    for s in range(1,nsamples-1):
        x = s/(nsamples-1)
        for i in range(max(0,int(o_to_s*s)-hat_support),min(C.shape[0],int(o_to_s*s)+hat_support)):
        #for i in range(C.shape[0]):
            xc = T[i]/dT
            xl = xc  if i == 0 else T[i-1]/dT
            xr = xc  if i >= (C.shape[0]-1) else T[i+1]/dT
            H = hat(x,xl,xc,xr)
            C_unif[s,:] += C[i,:] * H
    C_unif[-1] = C[0]        
    return C_unif


def resample_tau(C, nsamples, tau = 0.21, hat_support=20):
    """
    From Blu, Thierry, Philippe Thévenaz, and Michael Unser. 
    "Linear interpolation revitalized." 
    IEEE Transactions on Image Processing 13.5 (2004): 710-719.
    """
    F  = uniform(C,nsamples,hat_support)

    # Progressive formula for determining interpolation weights
    W = np.zeros((nsamples,F.shape[1]))
    W[0] = F[0]
    delta = tau/nsamples
    # delta = tau
    for i in range(1,nsamples):
         W[i,:] = delta/(1.0-delta)*W[i-1,:]+1.0/(1.0-delta)*F[i,:]
    W[0] = W[-1]        
    return W

def get_contours(fname, thr=10.0):
    img = imread(fname)
    lbl, _ = label(img)
    cc = measure.find_contours(lbl == lbl[200,200], .5)
    return cc[0] if len(cc) == 1 else None

#############################################
# Elliptical Fourier Descriptors of contour #
#############################################
def efd_contour(contour, harmonics=10):

    dxy = np.diff(contour, axis=0)
    dt = np.sqrt((dxy ** 2).sum(axis=1))
    t = np.concatenate([([0, ]), np.cumsum(dt)]).reshape(-1, 1)
    T = t[-1]

    phi = (2. * np.pi * t)/T

    coeffs = np.zeros((harmonics, 4))

    n = np.arange(1, harmonics + 1)
    const = T / (2 * n * n * np.pi * np.pi)
    phi_n = phi * n
    d_cos_phi_n = np.cos(phi_n[1:, :]) - np.cos(phi_n[:-1, :])
    d_sin_phi_n = np.sin(phi_n[1:, :]) - np.sin(phi_n[:-1, :])
    a_n = const * np.sum((dxy[:, 1] / dt).reshape(-1, 1) * d_cos_phi_n, axis=0)
    b_n = const * np.sum((dxy[:, 1] / dt).reshape(-1, 1) * d_sin_phi_n, axis=0)
    c_n = const * np.sum((dxy[:, 0] / dt).reshape(-1, 1) * d_cos_phi_n, axis=0)
    d_n = const * np.sum((dxy[:, 0] / dt).reshape(-1, 1) * d_sin_phi_n, axis=0)

    coeffs = np.vstack((a_n, b_n, c_n, d_n)).T
    
    return coeffs

def efd_inverse(coeffs, locus=(0,0), n_coords=200, harmonic=10):

    t = np.linspace(0, 1, n_coords).reshape(1, -1)
    n = np.arange(harmonic).reshape(-1, 1)

    xt = (np.matmul(coeffs[:harmonic, 2].reshape(1, -1),
                    np.cos(2. * (n + 1) * np.pi * t)) +
          np.matmul(coeffs[:harmonic, 3].reshape(1, -1),
                    np.sin(2. * (n + 1) * np.pi * t)) +
          locus[0])

    yt = (np.matmul(coeffs[:harmonic, 0].reshape(1, -1),
                    np.cos(2. * (n + 1) * np.pi * t)) +
          np.matmul(coeffs[:harmonic, 1].reshape(1, -1),
                    np.sin(2. * (n + 1) * np.pi * t)) +
      locus[1])

    return xt.ravel(), yt.ravel()


##########################
# CURVATURE CALCULATIONS #
##########################

def curvature_angles(contour):
    if all(contour[0,:] == contour[-1,:]):
        contour = contour[:-1,:]
    p1 = contour
    p2 = np.roll(contour, 1, axis=0)
    p3 = np.roll(contour, 2, axis=0)

    deg1 = (360 + np.rad2deg(np.arctan2(p1[:,0] - p2[:,0], p1[:,1] - p2[:,1]))) % 360
    deg2 = (360 + np.rad2deg(np.arctan2(p3[:,0] - p2[:,0], p3[:,1] - p2[:,1]))) % 360
    
    correction = (deg1 >= deg2) * 360
    
    return (deg2-deg1)+correction

# the angle is cumulative
def next_coord(x, y, theta, dist=1):
    theta = np.sum(np.pi-theta)
    
    return (x + dist*np.sin(theta), y + dist*np.cos(theta))

def reconstruct_from_curvature(curvature, mode='deg'):
    if mode == 'deg':
        curvature = np.deg2rad(curvature)
    elif mode == 'rad':
        pass
    else:
        raise("Only 'deg' (degrees) or 'rad' (radians) are available")
    p = (0,0)
    contour=[]
    for n in range(len(curvature)):
        p = next_coord(p[0], p[1], curvature[:n], dist=.33)
        contour.append(p)

    contour = scale_coordinates(np.array(contour))
    return contour

def contour_centroid(c):
    xs = c[:,0]
    ys = c[:,1]
    return np.sum(xs)/c.shape[0], np.sum(ys)/c.shape[0]

def scale_coordinates(c):
    cx,cy = contour_centroid(c)
    c[:,0] = c[:,0]- cx
    c[:,1] = c[:,1]- cy

    scaled = ((c - c.min()) * 2) / (c.max()-c.min()) -1
    
    return scaled

if __name__ == '__main__':

    files = glob('/g/korbel/cosenza/micronuclei_dataset/mni_dataset_2021-03/nucl_crops_atypic/*mask*.tif')
    closed_contours = []
    file_list = []
    for f in tqdm(files):
        c = get_contours(f)
        if (c is not None) and (is_closed(c)):
            closed_contours.append(c)
            file_list.append(f)

    # smoothening contours
    smoothed_contours = [ smoother(con,40) for con in closed_contours ]

    # resampling contours
    resampled_contours = [ resample_tau(con, 128, tau=0.21, hat_support=100) for con in smoothed_contours]

    # calculate curvatures
    curvatures = [curvature_angles(scale_coordinates(c)) for c in resampled_contours]

    # # elliptic fourier descriptors of curvature
    # efds = []
    # for c in tqdm(curvatures):
    #     # stack curvature vector with a vector of zeros to reuse an existing function
    #     c = np.concatenate([c, [c[0]]])
    #     v = np.stack((c, np.zeros_like(c)), axis=1)
    #     efd_out = elliptic_fourier_descriptors(v, order=10, normalize=True)
    #     efds.append(efd_out.flatten())
    
    # saving
    np.save('./file_list', file_list, allow_pickle=True)
    np.save('./resampled_contours', np.array(resampled_contours), allow_pickle=True)
    np.save('./curvatures', np.array(curvatures), allow_pickle=True)
    # np.save('./efds', np.array(efds), allow_pickle=True)