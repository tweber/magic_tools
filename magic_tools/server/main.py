#!/usr/bin/env python
# coding: utf-8
import os
import json
import logging
from datetime import datetime

from fastapi import FastAPI, Request, Response
from fastapi.middleware.wsgi import WSGIMiddleware
import pandas as pd
from skimage.filters import median
from skimage.morphology import disk

from .API_utils import ImgJson, encode_img, decode_img
from ..pipelines.micronuclei_pipelines import MicronucleiFeatures, MicronucleiFeaturesWatershed
from ..pipelines.micronuclei_pipelines import MicronucleiClassification, RoundClassification, RoundClassification_CombinedFeatures
from ..pipelines.pipeline import Configuration
from .dashboard import generate_dashboard_app
from .. import __path__


#from .dashboard import dashboard

# logging
date = datetime.now().strftime("%Y%m%d")
pkgdir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
LOGPATH = os.path.join(pkgdir, 'logs', '{}_magic_tools.log'.format(date))
os.makedirs(os.path.join(pkgdir, 'logs'), exist_ok=True)

logging.basicConfig(format='%(asctime)s,%(msecs)d\t%(levelname)-8s\t[%(filename)s:%(lineno)d]\t%(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    filename=LOGPATH,
    level=logging.DEBUG)

# TODO
# turn CONFIG into a dictionary with keys being the endpoints.
# this way we can load a different configuration for each endpoint
# and have the server perform multiple jobs without interfering with each other
CONFIG = Configuration()

app = FastAPI(debug=True, title='magic_tools server', version='0.0.1')

# mount dashboard
dash_app = generate_dashboard_app(logpath = LOGPATH)
app.mount("/dashboard", WSGIMiddleware(dash_app.server))


@app.on_event("startup")
def startup():
    pass

@app.post("/zenblue")
async def request_zenblue(IMG: ImgJson, request: Request):
    # test function for ZenBlue integration
    print('REQUEST ZENBLUE')
    IMG = dict(IMG)
    print(IMG.keys())

    return IMG

@app.post("/median")
async def request_median(IMG: ImgJson, request: Request):
    # test function applying median filter
 
    logging.info('request median from {}'.format(request.client.host))
    
    # decode image
    img = decode_img(IMG.img, IMG.width, IMG.height, IMG.dtype)

    # apply median filter
    med = median(img, selem=disk(5))

    # encode output
    out = encode_img(med)

    logging.debug('request median completed')

    return out


@app.post("/mni_features_full")
async def request_mni_features_full(IMG: ImgJson, request: Request):
    
    logging.info('request mni_features_full from {}'.format(request.client.host))
    
    # decode image
    img = decode_img(IMG.img, IMG.width, IMG.height, IMG.dtype)
    filename = IMG.filename
    print('FILENAME: {}'.format(filename))
    if len(CONFIG.workers.get_workers()) == 0:
        raise RuntimeError('workers not initialized')

    # generate features
    pipe = MicronucleiFeatures(CONFIG)
    pipe.run(img=img, filename=filename)

    mni_fts = pipe.get_output('mni_fts').to_csv()
    nucl_fts = pipe.get_output('nucl_fts').to_csv()

    nn = pipe.get_output('nn').to_csv()
    mni = encode_img(pipe.get_output('labels_mni'), filename).dict()
    nucl = encode_img(pipe.get_output('labels_nucl'), filename).dict()
    raw = encode_img(pipe.get_output('labels_raw'), filename).dict()
    json_out = json.dumps({'mni_fts': mni_fts, 'nucl_fts': nucl_fts, 'mni': mni, 'nucl': nucl, 'raw': raw, 'nn': nn}, indent = 4) 

    # encode output
    out = Response(content= json_out, media_type='text/json')
    
    logging.debug('request mni_features_full completed')
    
    return out

@app.post("/mni_features_watershed_full")
async def request_mni_features_watershed_full(IMG: ImgJson, request: Request):
    
    logging.info('request  mni_watershed_features from {}'.format(request.client.host))
    
    # decode image
    img = decode_img(IMG.img, IMG.width, IMG.height, IMG.dtype)
    filename = IMG.filename
    print('FILENAME: {}'.format(filename))
    if len(CONFIG.workers.get_workers()) == 0:
        raise RuntimeError('workers not initialized')

    # generate features
    pipe = MicronucleiFeaturesWatershed(CONFIG)
    pipe.run(img=img, filename=filename)

    mni_fts = pipe.get_output('mni_fts').to_csv()
    nucl_fts = pipe.get_output('nucl_fts').to_csv()

    nn = pipe.get_output('nn').to_csv()
    mni = encode_img(pipe.get_output('labels_mni'), filename).dict()
    nucl = encode_img(pipe.get_output('labels_nucl'), filename).dict()
    raw = encode_img(pipe.get_output('labels_raw'), filename).dict()
    wat = encode_img(pipe.get_output('watershed'), filename).dict()

    json_out = json.dumps({'mni_fts': mni_fts, 'nucl_fts': nucl_fts, 'mni': mni, 'nucl': nucl, 'raw': raw, 'wat': wat, 'nn': nn}, indent = 4) 

    # encode output
    out = Response(content= json_out, media_type='text/json')
    
    logging.debug('request mni_watershed_features completed')
    
    return out

@app.post("/set_config")
async def set_config(filepath: dict, request: Request):
    config = Configuration(filepath['filepath'])
    CONFIG.name = config.name
    CONFIG.options = config.options
    CONFIG.workers = config.workers

    logging.warning('SET CONFIGURATION {}'.format(CONFIG.name))
    return 

@app.post("/mni_classify")
async def request_mni_classify(IMG: ImgJson, request: Request):
    
    logging.info('request mni_classify from {}'.format(request.client.host))
    
    # decode image
    img = decode_img(IMG.img, IMG.width, IMG.height, IMG.dtype)
    filename = IMG.filename
    if filename is None:
        filename = 'image'
    print('FILENAME: {}'.format(filename), filename is None)
    
    if len(CONFIG.workers.get_workers()) == 0:
        raise RuntimeError('workers not initialized')

    # generate features
    pipe = MicronucleiClassification(CONFIG)
    pipe.run(img=img, filename=filename)

    out = encode_img(pipe.get_output('pred_mni'), filename).dict()
    
    logging.debug('request mni_classify completed')
    
    return out

@app.post("/round_classify")
async def request_round_classify(IMG: ImgJson, request: Request):
    
    logging.info('request round_classify from {}'.format(request.client.host))
    
    # decode image
    img = decode_img(IMG.img, IMG.width, IMG.height, IMG.dtype)
    filename = IMG.filename
    if filename is None:
        filename = 'image'
    print('FILENAME: {}'.format(filename), filename is None)
    
    if len(CONFIG.workers.get_workers()) == 0:
        raise RuntimeError('workers not initialized')

    # generate features
    pipe = RoundClassification(CONFIG)
    pipe.run(img=img, filename=filename)

    out = encode_img(pipe.get_output('pred_rnd'), filename).dict()
    
    logging.debug('request round_classify completed')
    
    return out


@app.post("/round_classify_combined_features")
async def request_round_classify_combined_features(IMG: ImgJson, request: Request):
    """
    Endpoint for performing round nuclei classification (that considers also features of protrusions).
    """
    logging.info('request round_classify_combined_features pipeline from {}'.format(request.client.host))
    
    # decode image
    img = decode_img(IMG.img, IMG.width, IMG.height, IMG.dtype)
    filename = IMG.filename
    if filename is None:
        filename = 'image'
    print('FILENAME: {}'.format(filename), '\t - filename is None:', filename is None)
    
    if len(CONFIG.workers.get_workers()) == 0:
        raise RuntimeError('workers not initialized')

    # generate features
    pipe = RoundClassification_CombinedFeatures(CONFIG)
    pipe.run(img=img, filename=filename)

    out = encode_img(pipe.get_output('pred_rnd'), filename).dict()
    
    logging.debug('request round_classify_combined_features completed')
    
    return out


@app.post("/round_combined_features")
async def request_round_combined_features(IMG: ImgJson, request: Request):
    """
    Endpoint for retrieving feature tables for round nuclei classification (that considers also features of protrusions).
    """
    logging.info('request round_combined_features pipeline from {}'.format(request.client.host))
    
    # decode image
    img = decode_img(IMG.img, IMG.width, IMG.height, IMG.dtype)
    filename = IMG.filename
    if filename is None:
        filename = 'image'
    print('FILENAME: {}'.format(filename), '\t - filename is None:', filename is None)
    
    if len(CONFIG.workers.get_workers()) == 0:
        raise RuntimeError('workers not initialized')

    # generate features
    pipe = RoundClassification_CombinedFeatures(CONFIG)
    pipe.run(img=img, filename=filename)

    out = pipe.get_output('nucl-mni_fts').to_csv()

    json_out = json.dumps({'nucl-mni_fts': out}, indent = 4) 

    # encode output
    out = Response(content= json_out, media_type='text/json')
    
    logging.debug('request round_combined_features completed')
    
    return out

