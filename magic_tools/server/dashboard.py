#!/usr/bin/env python
# coding: utf-8

import argparse
from PIL import Image
import os
import numpy as np
import pandas as pd
import plotly.express as px
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
from skimage.io import imread
from skimage.exposure import rescale_intensity

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def generate_dashboard_app(logpath, prefix='/dashboard/'):

    # create base figures
    FIGURES = {
        'execution_time': px.line(title='Job execution time', template='plotly_dark').update_layout(xaxis_title='experiment time', yaxis_title='time (s)'),
        'job_rate': px.line(title='Job frequency', template='plotly_dark').update_layout(xaxis_title='experiment time', yaxis_title='count'),
        'object_rate': px.line(title='Object detection frequency', template='plotly_dark').update_layout(xaxis_title='experiment time', yaxis_title='count'),
        'elapsed_time': px.line(title='Elapsed time since last job', template='plotly_dark').update_layout(xaxis_title='experiment time', yaxis_title='time (m)'),
        'probability_histogram': px.histogram(title='Object probability distribution', x=[], nbins=50, template='plotly_dark').update_layout(xaxis_title='probability', yaxis_title='count')
    }
    FIGURES['execution_time'].add_traces(data=[{'x':[], 'y':[]}, {'x':[], 'y':[]}])


    # define the app
    dashboard = dash.Dash('magic_tools server dashboard', requests_pathname_prefix=prefix)

    theme = {
        'background': '#111111',
        'text': '#EFEFEF',
        'title': '#16D984',
    }


    dashboard.layout = html.Div(style={'backgroundColor': theme['background']},
        children=[
            html.Div(
                    children=[
                        # SCALARS left side
                        html.Div(children=[html.H2('magic_tools server dashboard', style={'color': theme['title']}),
                                        html.P('Monitoring image analysis server'),
                                        html.Br(),
                                        html.Div(id='general_stats'), # callback
                                        html.Br(), html.Br(),
                                        html.Div(id='images')], # callback
                                style={'padding': 10, 'flex': 1, 'max-width': '600px'}), 
                        # GRAPHS right side
                        html.Div(children=[dcc.Graph(id='execution time', config={'displayModeBar': False}), # callback
                                        dcc.Graph(id='elapsed time', config={'displayModeBar': False}), # callback
                                        dcc.Graph(id='job rate', config={'displayModeBar': False}), # callback
                                        dcc.Graph(id='object probability', config={'displayModeBar': False}), # callback
                                        dcc.Graph(id='photoconversion rate', config={'displayModeBar': False})], # callback
                                style={'padding': 10, 'flex': 1, 'width': '800px', 'color': theme['text']}) 
                    ], style={'font-family': 'sans-serif', 'display': 'flex', 'flex-direction': 'row', 'color': theme['text']}),
            # interval updater
            dcc.Interval(id='updater', interval=20*1000, n_intervals=1),
            dcc.Store(id='logdata', data=dict())
            ]
    )

    @dashboard.callback(Output('logdata', 'data'),
                Input('updater', 'n_intervals'))
    def parse_logdata(n):
        #logpath = '/data/projects/magic_tools/20210421_img_API_clean.log'
        df = pd.read_csv(logpath,
                        names=['time', 'level', 'line', 'message', 'unk'],
                        sep='\t', header=None, on_bad_lines='skip')
        df['time'] = pd.to_datetime(df['time'], errors='coerce')
        df = df[~df['time'].isna()]
        df = df.fillna('')
        return df.to_dict()



    @dashboard.callback(Output('execution time', 'figure'),
                Input('logdata', 'data'))
    def chart_execution_time(data):
        '''generate plot for execution time for each job depending on their starting time'''

        logdata = pd.DataFrame().from_dict(data)
        #logdata['time'] = pd.to_datetime(logdata['time'])

        # create table
        class_exec_time = logdata[(logdata['message'].str.contains('request ').fillna(False))]
        class_exec_time['job_type'] = class_exec_time['message'].str.split(' ').str[1]
        class_exec_time['completed'] = class_exec_time['message'].str.contains('completed')
        class_exec_time['time'] = pd.to_datetime(class_exec_time['time'])
        class_exec_time['paired'] = class_exec_time['completed'] != np.roll(class_exec_time['completed'].values, 1)
        class_exec_time = class_exec_time[class_exec_time['paired']]
        class_exec_time[class_exec_time['completed']]['time']
        deltas = pd.DataFrame()
        deltas['execution time'] = (class_exec_time.loc[class_exec_time['completed'],'time'].values - class_exec_time.loc[~class_exec_time['completed'],'time'].values).astype(float)/1e9
        deltas['time'] = class_exec_time.loc[class_exec_time['completed'],'time'].values
        deltas['job_type'] = class_exec_time.loc[class_exec_time['completed'],'job_type'].values

        graph_dicts = []
        for job in deltas['job_type'].unique():
            job_dict = {
                'x': deltas[deltas['job_type'] == job]['time'],
                'y': deltas[deltas['job_type'] == job]['execution time'].values,
                'name': job, 'type': 'scatter'
                }
            graph_dicts.append(job_dict)
            
        # update figure
        FIGURES['execution_time'].update({
            'data': graph_dicts
        })
        return FIGURES['execution_time']



    @dashboard.callback(Output('job rate', 'figure'),
                        Input('logdata', 'data'))
    def chart_job_rate(data):
        '''generate plot for the number of jobs completed per hour'''

        logdata = pd.DataFrame().from_dict(data)

        # create table
        df = logdata[logdata['message'].str.contains('from').fillna(False)].reset_index()
        df['time'] = pd.to_datetime(df['time'])
        df = df.set_index(df['time'])
        df.rename(columns={'time': 'time2', 'level': 'jobs per hour'}, inplace=True)
        df = df.sort_index()
        df2 = df.rolling(window=pd.Timedelta('01:00:00')).agg('count')
        df2['job_type'] = 'all_jobs'
        df2 = df2.sort_values(by='time').reset_index()

        # update figure
        FIGURES['job_rate'].update({
            'data': [
                {
                    'x': df2['time'],
                    'y': df2['jobs per hour'],
                    'name': 'all', 'type': 'scatter'
                }]
        })
        
        return FIGURES['job_rate']
        


    @dashboard.callback(Output('photoconversion rate', 'figure'),
                        Input('logdata', 'data'))
    def chart_object_rate(data):
        '''generate plot of detected objects (photoconversions) per hour'''

        logdata = pd.DataFrame().from_dict(data)

        # create table
        df = logdata[(logdata['message'].str.contains('object detected'))].reset_index()
        # some times 2 concurrent log entries can result in a negative timedelta
        # raising an error at df.rolling, because it assumes a monotonically increasing index
        # this is solved by sorting the index
        df['time'] = pd.to_datetime(df['time'])
        df = df.set_index(df['time']).sort_index()
        df.rename(columns={'time': 'time2', 'level': 'photoconversions per hour'}, inplace=True)
        
        df2 = df.rolling(window=pd.Timedelta('01:00:00')).agg('count')
        df2 = df2.sort_values(by='time').reset_index()
        
        # update figure
        FIGURES['object_rate'].update({
            'data': [
                {
                    'x': df2['time'],
                    'y': df2['photoconversions per hour'],
                    'type': 'scatter'
                }]
        })
        
        return FIGURES['object_rate']

    def generate_table(logdata, job_regex, job_type):
        '''utility function to generate table of elapsed time between jobs for a certain job type'''

        logdata = pd.DataFrame().from_dict(logdata)

        df = logdata[(logdata['message'].str.contains(job_regex)) & (logdata['message'].str.contains('from'))].reset_index()
        if len(df) == 0:
            return df
        df['next_time'] = pd.concat([pd.DataFrame(df.iloc[0]).transpose(), df.iloc[:-1]]).reset_index()['time']

        df['job_length'] = pd.to_datetime(df['time']) - pd.to_datetime(df['next_time'])
        df['elapsed time (between jobs)'] = df['job_length'].dt.total_seconds()/60
        df['job_type'] = job_type
        return df



    @dashboard.callback(Output('elapsed time', 'figure'),
                        Input('logdata', 'data'))
    def chart_elapsed_time(data):
        '''generate plot about elapsed time between jobs of the same type'''

        logdata = pd.DataFrame().from_dict(data)

        # filter
        df = logdata[(logdata['message'].str.contains('request ').fillna(False))]
        df['job_type'] = df['message'].str.split(' ').str[1]

        # create table
        jobs = df['job_type'].unique()
        args = [{'job_regex': j, 'job_type': j} for j in jobs]
        tables = [generate_table(df, **a) for a in args]

        if len(tables) > 0:
            merge = pd.concat(tables, axis=0)
            merge['time'] = pd.to_datetime(merge['time'])
        else:
            merge = pd.DataFrame(columns=['job_type','time','elapsed time (between jobs)'])
        graph_dicts = []
        for job in jobs:
            job_dict = {
                'x': merge[merge['job_type'] == job]['time'],
                'y': merge[merge['job_type'] == job]['elapsed time (between jobs)'].values,
                'name': job, 'type': 'scatter'
                }
            graph_dicts.append(job_dict)
        
        # update figure
        FIGURES['elapsed_time'].update({
            'data': graph_dicts
        })
        
        return FIGURES['elapsed_time']



    def format_time_delta(time):
        '''calculate time delta from now and format it in a string'''

        delta = (pd.to_datetime("now") - pd.to_datetime(time) + pd.Timedelta('2 hours')).components

        h = delta.days*24 + delta.hours
        h, m, s = [str(c).zfill(2) for c in [h, delta.minutes, delta.seconds]]
        return '{}:{}:{}'.format(h,m,s)



    @dashboard.callback(Output('general_stats', 'children'),
                        Input('logdata', 'data'))
    def update_general_stats(data):
        '''calculate scalar values of interest and generate html elements describing them'''

        logdata = pd.DataFrame().from_dict(data)

        # counts
        job_count = logdata['message'].str.contains('completed').sum()
        mni_count = logdata['message'].str.contains('detected').sum()

        # elapsed time alert
        elapsed_time = format_time_delta(logdata['time'].iloc[-1])
        h,m,s = list(map(int,elapsed_time.split(':')))
        elapsed_color = 'red' if (m >= 5 or h > 0) else None
        sep = html.Tr([ html.Td(html.Hr(style={"width": "100%", "opacity": "unset"})), html.Td(html.Hr(style={"width": "100%", "opacity": "unset"}))])
        return [html.Tr([ html.Td('Detected objects'), html.Td('{}'.format(mni_count), style={'text-align':'right'}) ] ),
                sep,
                html.Tr([ html.Td('Job count'),      html.Td('{}'.format(job_count), style={'text-align':'right'}) ] ),
                sep,
                html.Tr([ html.Td('Elapsed time'), html.Td(elapsed_time, style={'text-align':'right', 'color':elapsed_color})]),
                sep,
                html.Tr([ html.Td('Running time'), html.Td(format_time_delta(logdata['time'].iloc[0]), style={'text-align':'right'})])]



    @dashboard.callback(Output('object probability', 'figure'),
                Input('logdata', 'data'))
    def chart_object_prob(data):
        '''generate histogram of probabilities associated to detected objects'''

        logdata = pd.DataFrame().from_dict(data)

        
        # create table
        probs = logdata[logdata['message'].str.contains('object detected')]['message'].str.rsplit(' ', n=1, expand=True)
        probs.rename(columns={0: 'message', 1:'probability'}, inplace=True)

        if 'probability' in probs.columns:
            probs['assigned probability'] = pd.to_numeric(probs['probability'], errors='coerce')
            probs['assigned probability'] = probs['assigned probability'][probs['assigned probability'].between(0,1)]
        else:
            probs['assigned probability'] = pd.NA

        # update figure
        FIGURES['probability_histogram'].update({
            'data': [
                {
                    'nbinsx': 50,
                    'x': probs['assigned probability'],
                    'type': 'histogram'
                }]
        })
        
        return FIGURES['probability_histogram']
    

    @dashboard.callback(Output('images', 'children'),
                        Input('logdata', 'data'))
    def update_images(data):
        '''calculate scalar values of interest and generate html elements describing them'''
        pkgdir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        savedir = os.path.join(pkgdir, 'logs/', 'images/')
        os.makedirs(savedir, exist_ok=True)
        if not os.path.exists(savedir):
            return html.Td([ html.Td('No images found: {}'.format(savedir))])
        files = sorted(os.listdir(savedir))[::-1]
        files = [f for f in files if '.png' in f]
        n = 0
        mosaic = []
        for i in range(len(files)//4):
            if i > 12:
                break
            cols = []
            for j in range(4):
                img = imread(os.path.join(savedir, files[n]))
                img = rescale_intensity(img, in_range=(0, img.max()*.75))
                cols.append(html.Td(html.Img(src=Image.fromarray(img).resize((100,100)))))
                n += 1
            row = html.Tr(cols)
            mosaic.append(row)
                
        return mosaic
    

    return dashboard

    


if __name__ == '__main__':

    # parse arguments
    ap = argparse.ArgumentParser()
    ap.add_argument('-l', '--log', required=True, help='filepath to log file')
    ap.add_argument('-H', '--host', required=False, help='host address')
    ap.add_argument('-P', '--port', required=False, help='port')

    args = vars(ap.parse_args())
    logpath = args['log']
    host = '127.0.0.1' if args['host'] is None else args['host']
    port = '8050' if args['port'] is None else args['port']

    dashboard = generate_dashboard_app(logpath=logpath, prefix=None)
    dashboard.run_server(host=host, port=port, debug=True)
