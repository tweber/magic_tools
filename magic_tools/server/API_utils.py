#!/usr/bin/env python
# coding: utf-8

import base64
import numpy as np
from pydantic import BaseModel

class ImgJson(BaseModel):
    img: str
    width: int
    height: int
    dtype: str
    channels: int = None
    filename: str = None
    classifier_path: str = None
    feature_table: int = 0

def decode_img(img_string, w, h, dtype, c=None):
    # decode from string
    img_decoded = base64.b64decode(img_string)

    # convert to numpy array
    if c is None:
        img_arr = np.frombuffer(img_decoded, dtype=dtype).reshape(h, w)
    else:
        img_arr = np.frombuffer(img_decoded, dtype=dtype).reshape(c, h, w)
    return img_arr


def encode_img(img, filename=None):
    # encode the image
    byte_content = base64.b64encode(img)  # to bytes
    string_content = byte_content.decode('utf-8')  # to string

    # build the Json scheme with pydantic
    if len(img.shape) == 3:
        json = ImgJson(img=string_content, width=img.shape[-1], height=img.shape[-2], channels=img.shape[-3],
                       dtype=img.dtype.str, filename=filename)
    elif len(img.shape) == 2:
        json = ImgJson(img=string_content, width=img.shape[-1], height=img.shape[-2], 
                       dtype=img.dtype.str, filename=filename)

    return json